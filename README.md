# Weather Forecast Application #

WeatherForecast application displays 5 days weather updates with 3 hour interval duration.

### What is this repository for? ###

This repositiry contains source code for WeatherForecast android application.
This application uses https://openweathermap.org/ api to get weather data.

![alt text](http://mobileappdev.in/SatyaGit/screenshot1.png)

### How do I get set up? ###

clone the project and import into your android studio (used android studio 3.0 RC1 )
and update your app key from https://openweathermap.org/ account in AppConstants.java

### Features ###

1. Home screen displays current location weather after allowing app permissions for location.
2. Current location will be fetched using Google play services api.
3. You can search the location of your choice to get weather updates.
4. Location search happens using Google Places api.
5. Using free-quota, limit 1000 requests/ 24 hours (https://developers.google.com/places/android-api/usage)
6. Upcoming weather data will be shown for every 3 hour duration, by default 5 results.
7. User can change his preferences in settings screen, how many (5,10,20) upcoming weather results can be shown in app.
8. Last fetched place and weather info saved to local database.
9. if there is no internet connection, last updated data will be shown in dashboard.
10. The location/weather info saved and will be shown in next relaunch (Not always current location)

[download debug apk](http://mobileappdev.in/SatyaGit/weather-forcast-debug.apk)


### Tests ####
Run the tests using command ./gradlew test

### Contact ###
Any queries regarding this app, reach me on my mail id : satya@mobileappdev.in