package in.mobileappdev.weatherforecast.dashboard.presenter;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowSQLiteConnection;

import in.mobileappdev.weatherforecast.BuildConfig;
import in.mobileappdev.weatherforecast.TestUtils;
import in.mobileappdev.weatherforecast.dashboard.view.WeatherResponseView;
import in.mobileappdev.weatherforecast.shadows.AppUtilsShadow;
import in.mobileappdev.weatherforecast.utils.model.WeatherAPIResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.RuntimeEnvironment.application;

/**
 * Created by satyanayaran on 14/10/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, shadows = {AppUtilsShadow.class})
public class WeatherDataPresenterTest {

    private WeatherResponseView weatherResponseView;
    private WeatherDataPresenter weatherDataPresenter;
    private WeatherAPIResponse weatherAPIResponse;

    @Before
    public void setUp() throws Exception {
        ShadowSQLiteConnection.reset();
        weatherResponseView = mock(WeatherResponseView.class);
        weatherAPIResponse = mock(WeatherAPIResponse.class);
        weatherDataPresenter =new WeatherDataPresenter(application, weatherResponseView);


    }

    private WeatherAPIResponse getMockedWeatherAPIResponce() {
        String sampleJson = new TestUtils().readString("forecast.json");
        assertNotNull(sampleJson);
        try {
            Gson gson = new Gson();
             weatherAPIResponse =   gson.fromJson(sampleJson, WeatherAPIResponse.class);
        } catch (Exception e){
            fail("Mock json object creation failed");
        }

        return weatherAPIResponse;
    }

    @After
    public void tearDown() throws Exception {
        weatherDataPresenter.resetDBTables();
        weatherDataPresenter.resetDb();
        weatherDataPresenter=null;
    }

    @Test
    public void getWeatherResponseByLatLongWithOffline() throws Exception {
        weatherDataPresenter.storeWeatherInfoToOfflineDb(getMockedWeatherAPIResponce());
        AppUtilsShadow.isInterNetConnectionAvailable = false;
        weatherDataPresenter.getWeatherResponseByLatLong("22.123", "19.00");
        verify(weatherResponseView).onError(anyString(), anyBoolean());
    }

    @Test
    public void storeWeatherInfoToOfflineDbWithNull(){
        weatherDataPresenter.storeWeatherInfoToOfflineDb(null);
        assertEquals(0, weatherDataPresenter.getOfflineDbRowCount());
    }

    @Test
    public void storeWeatherInfoToOfflineDbWithInvalidJson(){
        WeatherAPIResponse weatherAPIResponse = new WeatherAPIResponse();
        weatherDataPresenter.storeWeatherInfoToOfflineDb(weatherAPIResponse);
        assertEquals(0, weatherDataPresenter.getOfflineDbRowCount());
    }

    @Test
    public void storeWeatherInfoToOfflineDbWithValidRespo(){
        WeatherAPIResponse weatherAPIResponse = getMockedWeatherAPIResponce();
        weatherDataPresenter.storeWeatherInfoToOfflineDb(weatherAPIResponse);
        assertEquals(4, weatherDataPresenter.getOfflineDbRowCount());
    }

}