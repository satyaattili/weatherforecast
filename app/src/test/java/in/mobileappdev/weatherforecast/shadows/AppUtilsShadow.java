package in.mobileappdev.weatherforecast.shadows;

import android.content.Context;

import org.robolectric.annotation.Implementation;
import org.robolectric.annotation.Implements;

import in.mobileappdev.weatherforecast.utils.AppUtils;

/**
 * Created by satyanayaran on 15/10/17.
 */

@Implements(AppUtils.class)
public class AppUtilsShadow {

    public static boolean isInterNetConnectionAvailable;

    @SuppressWarnings("unused")
    @Implementation
    public static boolean isInternetConnectionAvailable(Context context) {
        return isInterNetConnectionAvailable;
    }
}
