package in.mobileappdev.weatherforecast.dashboard.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

import in.mobileappdev.weatherforecast.R;
import in.mobileappdev.weatherforecast.dashboard.view.WeatherResponseView;
import in.mobileappdev.weatherforecast.db.AppDatabase;
import in.mobileappdev.weatherforecast.rest.RestClient;
import in.mobileappdev.weatherforecast.utils.AppConstants;
import in.mobileappdev.weatherforecast.utils.AppUtils;
import in.mobileappdev.weatherforecast.utils.model.City;
import in.mobileappdev.weatherforecast.utils.model.List;
import in.mobileappdev.weatherforecast.utils.model.Weather;
import in.mobileappdev.weatherforecast.utils.model.WeatherAPIResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by satyanayaran on 11/10/17.
 */

public class WeatherDataPresenter {

    private static final String TAG = WeatherDataPresenter.class.getSimpleName();
    private WeatherResponseView weatherResponseView;
    private RestClient restClient;
    private Context context;
    private AppDatabase appDatabase;

    private Callback<WeatherAPIResponse> weatherAPIResponseListner = new Callback<WeatherAPIResponse>() {
        @Override
        public void onResponse(@NonNull Call<WeatherAPIResponse> call, @NonNull Response<WeatherAPIResponse> response) {
            weatherResponseView.hideProgress();
            if (response.errorBody() != null) {
                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    weatherResponseView.onError(jObjError.getString("message"), false);
                } catch (Exception e) {
                    weatherResponseView.onError(context.getString(R.string.error_generic), false);
                }

                return;
            }

            WeatherAPIResponse weatherAPIResponse = response.body();

            if (weatherAPIResponse != null && weatherAPIResponse.getList() != null && weatherAPIResponse.getList().size()>0) {
                Log.d(TAG, "weatherAPIResponseListner COUNT : " + weatherAPIResponse.getCnt());
                weatherResponseView.onWeatherInfoRecieved(weatherAPIResponse);
                java.util.List<List> lists = weatherAPIResponse.getList();
                if(lists.size() > 2){
                    weatherResponseView.onHistoryReceived(lists.subList(1, lists.size()));
                }

                storeWeatherInfoToOfflineDb(weatherAPIResponse);
            }
        }

        @Override
        public void onFailure(Call<WeatherAPIResponse> call, Throwable t) {
            weatherResponseView.hideProgress();
            weatherResponseView.onError(t.getMessage(), false);
        }
    };


    public WeatherDataPresenter(Context context, WeatherResponseView weatherResponseView) {
        this.weatherResponseView = weatherResponseView;
        this.context = context;
        restClient = new RestClient();
        appDatabase = AppDatabase.getAppDatabase(context);
    }


    /**
     * make rest call with latitude, longitude
     *
     * @param latitude
     * @param longitude
     */
    public void getWeatherResponseByLatLong(String latitude, String longitude) {
        weatherResponseView.showProgress();
        //first loading local db data
        boolean errorScreen = false;
        if(!fillOfflineData()){
            errorScreen = true;
        }

        if (checkNetworkConnection(errorScreen)){
            weatherResponseView.hideProgress();
            return;
        }

        Call<WeatherAPIResponse> respo = restClient.getApiService().getWeatherInfoByLatLong(latitude, longitude, AppConstants.UNITS_METRIC, AppConstants.APP_ID);
        respo.enqueue(weatherAPIResponseListner);
    }

    /**
     * check internet connection, disable swipe refreshing on offline mode.
     *
     * @return
     */
    private boolean checkNetworkConnection(boolean errorScrren) {
        if (!AppUtils.isInternetConnectionAvailable(context)) {
            weatherResponseView.onError(context.getString(R.string.no_connection_msg), errorScrren);
            //Toast.makeText(context, R.string.no_connection_msg, Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    /**
     * Load Locally stored data first while fetching from server
     */
    private boolean fillOfflineData() {
        boolean dataAvailable = false;
        WeatherAPIResponse weatherAPIResponse = new WeatherAPIResponse();
        ArrayList<List> offlineData = (ArrayList<List>) appDatabase.listDao().getAllLists();
        for (List list : offlineData) {
            java.util.List<Weather> offlineWeather = appDatabase.weatherDao().getWeathersWithListId(list.getLid());
            list.setWeather(offlineWeather);
            weatherAPIResponse.setList(offlineData);
        }
        java.util.List<City> cities = appDatabase.cityDao().getCities();
        if (cities.size() > 0) {
            weatherAPIResponse.setCity(cities.get(0));
            dataAvailable = true;
        }
        if(offlineData.size()>1){
            weatherResponseView.onWeatherInfoRecieved(weatherAPIResponse);
            weatherResponseView.onHistoryReceived(offlineData.subList(1, offlineData.size()));
        }

        return dataAvailable;

    }




    /**
     * Saving weather info to DB
     *
     * @param weatherAPIResponse
     */
    protected void storeWeatherInfoToOfflineDb(WeatherAPIResponse weatherAPIResponse) {
        //storing in to db offline

        if(null == weatherAPIResponse){
            return;
        }
        resetDBTables();
        java.util.List<List> lists = weatherAPIResponse.getList();
        if(null == lists){
            return;
        }
        int size = lists.size() > AppConstants.offlineRecordsLimit ? AppConstants.offlineRecordsLimit : lists.size();
        for (int i = 0; i < size; i++) {
            long id = appDatabase.listDao().insertList(lists.get(i));
            for (Weather weather : lists.get(i).getWeather()) {
                Log.d(TAG, "Inserted LIST ID : " + id);
                //foriegn key relation with the table, so setting inserted list id to weather
                weather.setLid(id);
                appDatabase.weatherDao().insertAll(weather);
            }
        }
        City city = weatherAPIResponse.getCity();
        if (city != null) {
            appDatabase.cityDao().insert(weatherAPIResponse.getCity());
        }
    }

    /**
     * Deleting content from tables
     * //made protected to access in unit tests,
     */
    protected void resetDBTables() {
        if(null == appDatabase){
            return;
        }
        appDatabase.weatherDao().deleteTable();
        appDatabase.listDao().deleteTable();
        appDatabase.cityDao().deleteTable();
    }

    public int getOfflineDbRowCount(){
        if(null == appDatabase){
            return 0;
        }
        return appDatabase.listDao().countListItems();
    }

    protected void resetDb(){
        AppDatabase.destroyInstance();
    }

}
