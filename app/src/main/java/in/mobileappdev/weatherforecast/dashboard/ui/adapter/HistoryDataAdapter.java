package in.mobileappdev.weatherforecast.dashboard.ui.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.mobileappdev.weatherforecast.R;
import in.mobileappdev.weatherforecast.utils.AppConstants;
import in.mobileappdev.weatherforecast.utils.AppUtils;
import in.mobileappdev.weatherforecast.utils.model.List;
import in.mobileappdev.weatherforecast.utils.model.Weather;

/**
 * Created by satyanayaran on 11/10/17.
 */

public class HistoryDataAdapter extends RecyclerView.Adapter<HistoryDataAdapter.HistoryViewHolder> {

    private final Context context;
    private java.util.List<List> weatherArrayList;
    private  int itemsTodisplay;


    public HistoryDataAdapter(Context context, java.util.List<List> weatherArrayList, int itemsTodisplay) {
        this.context = context;
        this.weatherArrayList = weatherArrayList;
        this.itemsTodisplay = itemsTodisplay;
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_grid, parent, false);
        return new HistoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        List list = weatherArrayList.get(position);

        Weather weather = list.getWeather().get(0);

        holder.weatherInfo.setText(weather.getDescription());
        holder.weatherTime.setText(AppUtils.getWeekDayTime(list.getDtTxt()));
        Glide.with(context).load(AppConstants.IMAGE_BASE_URL.concat(weather.getIcon()).concat(".png"))
                .thumbnail(0.5f)
                .into(holder.weatherImage);

    }

    @Override
    public int getItemCount() {
        return weatherArrayList.size()>itemsTodisplay ? itemsTodisplay : weatherArrayList.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_item_icon)
        AppCompatImageView weatherImage;

        @BindView(R.id.txt_item_temperatue)
        AppCompatTextView weatherInfo;

        @BindView(R.id.txt_item_date_time)
        AppCompatTextView weatherTime;

        public HistoryViewHolder(View itemView) {
            super(itemView);
            //This is the devil :-)
            ButterKnife.bind(this,itemView);
        }
    }
}
