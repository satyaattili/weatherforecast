package in.mobileappdev.weatherforecast.dashboard.view;

import in.mobileappdev.weatherforecast.utils.model.List;
import in.mobileappdev.weatherforecast.utils.model.WeatherAPIResponse;

/**
 * Created by satyanayaran on 11/10/17.
 */

public interface WeatherResponseView {

    void showProgress();
    void hideProgress();
    void onWeatherInfoRecieved(WeatherAPIResponse weatherAPIResponse);
    void onHistoryReceived(java.util.List<List> historyList);
    void onError(String errorMessage, boolean errorScreen);



}
