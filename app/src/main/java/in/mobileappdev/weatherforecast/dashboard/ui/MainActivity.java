package in.mobileappdev.weatherforecast.dashboard.ui;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crash.FirebaseCrash;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.mobileappdev.weatherforecast.R;
import in.mobileappdev.weatherforecast.dashboard.presenter.WeatherDataPresenter;
import in.mobileappdev.weatherforecast.dashboard.ui.adapter.HistoryDataAdapter;
import in.mobileappdev.weatherforecast.dashboard.view.WeatherResponseView;
import in.mobileappdev.weatherforecast.utils.AppConstants;
import in.mobileappdev.weatherforecast.utils.AppUtils;
import in.mobileappdev.weatherforecast.utils.SharedPref;
import in.mobileappdev.weatherforecast.utils.model.City;
import in.mobileappdev.weatherforecast.utils.model.Main;
import in.mobileappdev.weatherforecast.utils.model.Weather;
import in.mobileappdev.weatherforecast.utils.model.WeatherAPIResponse;

public class MainActivity extends AppCompatActivity implements WeatherResponseView, SwipeRefreshLayout.OnRefreshListener, GoogleApiClient.ConnectionCallbacks {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 11;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final int PERMISSION_REQUEST_CODE = 100;
    private final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.img_banner_status_icon)
    AppCompatImageView weatherIcon;
    @BindView(R.id.txt_banner_temperature)
    AppCompatTextView weatherInfo;
    @BindView(R.id.lv_history)
    RecyclerView historyReclerView;
    @BindView(R.id.txt_banner_date_time)
    AppCompatTextView weatherUpdatedTime;
    @BindView(R.id.txt_banner_descriprtion)
    AppCompatTextView weatherDescription;
    @BindView(R.id.txt_banner_min_max_temp)
    AppCompatTextView minMaxTemp;
    @BindView(R.id.txt_location)
    AppCompatTextView tempLocation;

    @BindView(R.id.txt_pressure)
    AppCompatTextView pressureVal;
    @BindView(R.id.txt_humidity)
    AppCompatTextView humidityVal;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.error_layout)
    RelativeLayout errorLayout;
    @BindView(R.id.title_upcoming_weather)
    LinearLayout upcomingWeatherTitle;

    private WeatherDataPresenter weatherDataPresenter;
    private GoogleApiClient mGoogleApiClient;
    private LatLng latLngFromSharedPref;
    private SharedPreferences sharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setTitle(R.string.title_dashboard);
        }

        FirebaseApp.initializeApp(this);
        SharedPref.init(this);
        swipeRefreshLayout.setOnRefreshListener(this);

        sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);

        weatherDataPresenter = new WeatherDataPresenter(this, this);
        latLngFromSharedPref = getLatLongFromSharedPreference();
        intialiseWeatherRequest();

    }


    private void intialiseWeatherRequest() {
        latLngFromSharedPref = getLatLongFromSharedPreference();
        if (null != latLngFromSharedPref) {
            makeCallToGetResult(latLngFromSharedPref.latitude, latLngFromSharedPref.longitude);
        } else {
            getCurrentLocationUsingGoogleServices();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    callPlaceDetectionApi();
                }
                break;
        }
    }

    private void callPlaceDetectionApi() throws SecurityException {

        showProgress();
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                hideProgress();
                if (likelyPlaces != null && likelyPlaces.getCount() > 0) {
                    Place place = likelyPlaces.get(0).getPlace();
                    makeCallToGetResult(place);
                    likelyPlaces.release();
                } else {
                    Toast.makeText(MainActivity.this, "Places not found", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public void showProgress() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }

    }

    @Override
    public void hideProgress() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

    }


    @Override
    public void onWeatherInfoRecieved(WeatherAPIResponse weatherAPIResponse) {
        hideErrorScreen();
        bindWeatherInfoDataToViews(weatherAPIResponse);
    }

    /**
     * Helper method to fill the data received from server
     *
     * @param weatherAPIResponse
     */
    private void bindWeatherInfoDataToViews(WeatherAPIResponse weatherAPIResponse) {
        //Showing first item info for dashboard
        ArrayList<in.mobileappdev.weatherforecast.utils.model.List> lists = weatherAPIResponse.getList();
        if (lists == null || lists.size() == 0) {
            return;
        }
        in.mobileappdev.weatherforecast.utils.model.List weatherList = weatherAPIResponse.getList().get(0);
        Weather weather = weatherList.getWeather().get(0);
        if (weather != null) {
            weatherDescription.setText(weather.getDescription());
            Glide.with(this).load(AppConstants.IMAGE_BASE_URL + weather.getIcon() + ".png")
                    .thumbnail(0.5f)
                    .into(weatherIcon);
        }

        Main mainData = weatherList.getMain();
        if (mainData != null) {
            String temp = String.valueOf(mainData.getTemp());
            weatherInfo.setText(AppUtils.getStringInDegrees(temp));
            minMaxTemp.setText(String.format("%s / %s", AppUtils.getStringInDegrees(String.valueOf(mainData.getTempMin())), AppUtils.getStringInDegrees(String.valueOf(mainData.getTempMax()))));
            pressureVal.setText(String.format("Pressure \n %s", mainData.getPressure()));
            humidityVal.setText(String.format("Humidity \n %s", mainData.getHumidity()));
        }

        City city = weatherAPIResponse.getCity();
        if (city != null) {
            tempLocation.setText(weatherAPIResponse.getCity().getName());
        }

        weatherUpdatedTime.setText(AppUtils.getDate(weatherList.getDtTxt()));

    }


    @Override
    public void onHistoryReceived(List<in.mobileappdev.weatherforecast.utils.model.List> historyList) {
        if (historyList.size() == 0) {
            upcomingWeatherTitle.setVisibility(View.GONE);
            return;
        }
        upcomingWeatherTitle.setVisibility(View.VISIBLE);
        int upcomingWeatherCount = Integer.parseInt(sharedPrefs.getString("weather_count", "5"));
        HistoryDataAdapter adapter = new HistoryDataAdapter(this, historyList, upcomingWeatherCount);
        historyReclerView.setLayoutManager(new GridLayoutManager(this, 2));
        historyReclerView.setAdapter(adapter);
    }

    @Override
    public void onError(String errorMessage, boolean errorScreen) {
        if(errorScreen){
            showErrorScreen();
        }else{
            Toast.makeText(this, R.string.error_generic, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRefresh() {
        if (null == latLngFromSharedPref || !AppUtils.isInternetConnectionAvailable(this)) {
            hideProgress();
            return;
        }
        makeCallToGetResult(latLngFromSharedPref.latitude, latLngFromSharedPref.longitude);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_select_location) {
            selectCityByUsingGooglePlacesApi();
        } else if(id == R.id.action_current_location){
            getCurrentLocationUsingGoogleServices();
        } else{
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Starting Google Play services plugin to select location
     */
    private void selectCityByUsingGooglePlacesApi() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            FirebaseCrash.report(e);
            AppUtils.showAlertDialog(this, getString(R.string.dialog_title_play_services), getString(R.string.dialog_msg_playservices));
        } catch (GooglePlayServicesNotAvailableException e) {
            FirebaseCrash.report(e);
            AppUtils.showAlertDialog(this, getString(R.string.dialog_title_play_services), getString(R.string.dialog_msg_playservices));
        }
    }

    /**
     * Get current location using Google play services
     */
    private void getCurrentLocationUsingGoogleServices() {

        if (!AppUtils.isInternetConnectionAvailable(this)) {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show();
            if(weatherDataPresenter.getOfflineDbRowCount() == 0){
                onError(getString(R.string.no_data_found_try_again), true);
            }
            return;
        }

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addConnectionCallbacks(this)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            FirebaseCrash.log(connectionResult.getErrorMessage());
                        }
                    }).build();
        }

        if (mGoogleApiClient.isConnected()) {
            int hasPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            } else {
                callPlaceDetectionApi();
            }
        } else {
            mGoogleApiClient.connect();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                makeCallToGetResult(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                FirebaseCrash.log(status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, R.string.toast_location_selection_cancelled, Toast.LENGTH_LONG).show();
            }
        }
    }


    /**
     * make presenter call to get weather data by latlong
     *
     * @param latitude
     * @param longitude
     */
    private void makeCallToGetResult(double latitude, double longitude) {
        SharedPref.write(AppConstants.SP_CURRENT_LOC_LAT, String.valueOf(latitude));
        SharedPref.write(AppConstants.SP_CURRENT_LOC_LONG, String.valueOf(longitude));
        weatherDataPresenter.getWeatherResponseByLatLong(String.valueOf(latitude), String.valueOf(longitude));
    }


    /**
     * make presenter call to get weather data by using Place
     *
     * @param place
     */
    private void makeCallToGetResult(Place place) {
        SharedPref.write(AppConstants.SP_CURRENT_LOC_LAT, String.valueOf(place.getLatLng().latitude));
        SharedPref.write(AppConstants.SP_CURRENT_LOC_LONG, String.valueOf(place.getLatLng().longitude));
        weatherDataPresenter.getWeatherResponseByLatLong(String.valueOf(place.getLatLng().latitude), String.valueOf(place.getLatLng().longitude));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocationUsingGoogleServices();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Get langitude, latitude from prefernces
     * @return
     */
    private LatLng getLatLongFromSharedPreference() {
        try {
            String latitude = SharedPref.read(AppConstants.SP_CURRENT_LOC_LAT, null);
            String longitude = SharedPref.read(AppConstants.SP_CURRENT_LOC_LONG, null);
            if (latitude != null && longitude != null) {
                return new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
            }

        } catch (NumberFormatException e) {
            FirebaseCrash.logcat(Log.ERROR, TAG, getString(R.string.npe));
            FirebaseCrash.report(e);
        }

        return null;
    }


    private void showErrorScreen(){
        swipeRefreshLayout.setVisibility(View.GONE);
        errorLayout.setVisibility(View.VISIBLE);
    }

    private void hideErrorScreen(){
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_tryagain)
    public void onButtonClick(){
        if(null == weatherDataPresenter){
            return;
        }

        intialiseWeatherRequest();
    }
}
