package in.mobileappdev.weatherforecast.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import in.mobileappdev.weatherforecast.utils.model.List;

/**
 * Created by satyanayaran on 13/10/17.
 */

@Dao
public interface ListDao {

    @Insert
    long insertList(List list);

    @Insert
    long[] insertMultipleLists(java.util.List<List> lists);

    @Query("SELECT * FROM list")
    java.util.List<List> getAllLists();

    @Delete
    void delete(List main);

    @Query("SELECT COUNT(*) from list")
    int countListItems();


    @Query("DELETE FROM list")
    void deleteTable();
}
