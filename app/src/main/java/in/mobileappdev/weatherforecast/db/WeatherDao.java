package in.mobileappdev.weatherforecast.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.mobileappdev.weatherforecast.utils.model.Weather;

/**
 * Created by satyanayaran on 12/10/17.
 */

@Dao
public interface WeatherDao {

    @Insert
    void insertAll(Weather... weathers);

    @Query("SELECT * FROM weather")
    List<Weather> getAllWeathers();

    @Query("SELECT * FROM weather WHERE lid = :id ")
    List<Weather> getWeathersWithListId(long id);

    @Query("DELETE FROM weather")
    void deleteTable();
}
