package in.mobileappdev.weatherforecast.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import in.mobileappdev.weatherforecast.utils.model.City;
import in.mobileappdev.weatherforecast.utils.model.List;
import in.mobileappdev.weatherforecast.utils.model.Main;
import in.mobileappdev.weatherforecast.utils.model.Weather;

/**
 * Created by satyanayaran on 12/10/17.
 */

@Database(entities = {Weather.class, Main.class, City.class, List.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{

    private static AppDatabase INSTANCE;
    public abstract WeatherDao weatherDao();
    public abstract ListDao listDao();
    public abstract CityDao cityDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
