package in.mobileappdev.weatherforecast.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import in.mobileappdev.weatherforecast.utils.model.City;

/**
 * Created by satyanayaran on 13/10/17.
 */

@Dao
public interface CityDao {

    @Insert
    void insert(City... city);

    @Query("SELECT * FROM City")
    List<City> getCities();

    @Query("DELETE FROM City")
    void deleteTable();
}
