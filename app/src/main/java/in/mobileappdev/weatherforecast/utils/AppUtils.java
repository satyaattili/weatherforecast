package in.mobileappdev.weatherforecast.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import in.mobileappdev.weatherforecast.R;

/**
 * Created by satyanayaran on 11/10/17.
 */

public class AppUtils {

    public static boolean isInternetConnectionAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        return false;
    }

    public static boolean isEmptyString(String string){
       return string != null && !string.equals("");
    }

// --Commented out by Inspection START (16/10/17, 2:42 PM):
//    public static String generateQueryStringByCityId(String city, String country) {
//        return  (isEmptyString(country)) ? city : city+","+country;
//    }
// --Commented out by Inspection STOP (16/10/17, 2:42 PM)

    public static String getDate(String date){
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date newDate;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
        spf= new SimpleDateFormat("EEE, d MMM yyyy", Locale.ENGLISH);
        date = spf.format(newDate);
        return date;
    }


    public static String getWeekDayTime(String date){
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date newDate;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
        spf= new SimpleDateFormat("EEEE, d, \n hh:mm aaa", Locale.ENGLISH);
        date = spf.format(newDate);
        return date;
    }

    public static String getStringInDegrees(String temp){
        return temp + (char) 0x00B0;
    }

    public static void showAlertDialog(Context contex, String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(contex);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        builder.show();
    }


}
