
package in.mobileappdev.weatherforecast.utils.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


@Entity(tableName = "weather")
public class WeatherAPIResponse {

    @PrimaryKey(autoGenerate = true)
    private int responseId;

    @Ignore
    @SerializedName("cod")
    @Expose
    private String cod;

    @Ignore
    @SerializedName("message")
    @Expose
    private Double message;

    @SerializedName("cnt")
    @Expose
    @Ignore
    private Integer cnt;

    @Ignore
    @SerializedName("list")
    @Expose
    private ArrayList<List> list = null;

    @ColumnInfo(name = "city")
    @SerializedName("city")
    @Expose
    private City city;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public ArrayList<List> getList() {
        return list;
    }

    public void setList(ArrayList<List> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

}
