
package in.mobileappdev.weatherforecast.utils.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class List {

    @PrimaryKey(autoGenerate = true)
    private int lid;

    @Ignore
    @SerializedName("dt")
    @Expose
    private Integer dt;

    @SerializedName("main")
    @Expose
    @Embedded
    private Main main;

    @SerializedName("weather")
    @Expose
    @Ignore
    private java.util.List<Weather> weather = null;

    @SerializedName("clouds")
    @Expose
    @Ignore
    private Clouds clouds;

    @SerializedName("wind")
    @Expose
    @Ignore
    private Wind wind;

    @SerializedName("sys")
    @Expose
    @Ignore
    private Sys sys;

    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;

    @SerializedName("rain")
    @Expose
    @Ignore
    private Rain rain;

    @SerializedName("snow")
    @Expose
    @Ignore
    private Snow snow;

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public java.util.List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(java.util.List<Weather> weather) {
        this.weather = weather;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

    public Rain getRain() {
        return rain;
    }

    public void setRain(Rain rain) {
        this.rain = rain;
    }

    public Snow getSnow() {
        return snow;
    }

    public void setSnow(Snow snow) {
        this.snow = snow;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }
}
