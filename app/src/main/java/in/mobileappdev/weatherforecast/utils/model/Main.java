
package in.mobileappdev.weatherforecast.utils.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Main {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "temp")
    @SerializedName("temp")
    @Expose
    private Double temp;

    @ColumnInfo(name = "temp_min")
    @SerializedName("temp_min")
    @Expose
    private Double tempMin;

    @ColumnInfo(name = "temp_max")
    @SerializedName("temp_max")
    @Expose
    private Double tempMax;

    @ColumnInfo(name = "pressure")
    @SerializedName("pressure")
    @Expose
    private Double pressure;

    @ColumnInfo(name = "sea_level")
    @SerializedName("sea_level")
    @Expose
    private Double seaLevel;

    @ColumnInfo(name = "grnd_level")
    @SerializedName("grnd_level")
    @Expose
    private Double grndLevel;

    @ColumnInfo(name = "humidity")
    @SerializedName("humidity")
    @Expose
    private Double humidity;

    @Ignore
    @SerializedName("temp_kf")
    @Expose
    private Double tempKf;

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public void setTempMin(Double tempMin) {
        this.tempMin = tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public void setTempMax(Double tempMax) {
        this.tempMax = tempMax;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getSeaLevel() {
        return seaLevel;
    }

    public void setSeaLevel(Double seaLevel) {
        this.seaLevel = seaLevel;
    }

    public Double getGrndLevel() {
        return grndLevel;
    }

    public void setGrndLevel(Double grndLevel) {
        this.grndLevel = grndLevel;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getTempKf() {
        return tempKf;
    }

    public void setTempKf(Double tempKf) {
        this.tempKf = tempKf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
