package in.mobileappdev.weatherforecast.utils;

/**
 * Created by satyanayaran on 11/10/17.
 */

public class AppConstants {
    public static final String APP_ID = "44e4004e3149255f2a3b50a9363c433b";
    public static final String POST_KEY = "appid";
    public static final String POST_Q = "q";

    public static final String POST_LAT = "lat";
    public static final String POST_LONG = "lon";
    public static final String POST_UNITS = "units";
    public static final String IMAGE_BASE_URL = "http://openweathermap.org/img/w/";

    public static final String SP_CURRENT_LOC_LAT = "pref_lat";
    public static final String SP_CURRENT_LOC_LONG = "pref_long";

    //offline . just want to show last updated weather info. thats why just limitted to 5 records,  to minimize the db size
    public static final int offlineRecordsLimit = 5;

    public static final String BASE_URL = "http://api.openweathermap.org/";
    public static final String UNITS_METRIC = "metric";

}
