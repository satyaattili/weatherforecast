package in.mobileappdev.weatherforecast.rest;

import in.mobileappdev.weatherforecast.utils.AppConstants;
import in.mobileappdev.weatherforecast.utils.model.WeatherAPIResponse;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by satyanayaran on 10/10/17.
 */

public interface ApiService {

    @POST("data/2.5/forecast")
    Call<WeatherAPIResponse> getWeatherInfoByLocation(@Query(AppConstants.POST_Q) String q, @Query(AppConstants.POST_UNITS) String units, @Query(AppConstants.POST_KEY) String appid);

    @POST("data/2.5/forecast")
    Call<WeatherAPIResponse> getWeatherInfoByLatLong(@Query(AppConstants.POST_LAT) String latitude, @Query(AppConstants.POST_LONG) String longitude, @Query(AppConstants.POST_UNITS) String units, @Query(AppConstants.POST_KEY) String appid);
}
